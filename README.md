
# Group Member Role

This module basically listens for Group Relation creation/deletion and synchronizes
a role. This provides site building convenience.

* If a user is added to a group, add a configured role.
* If a user is removed from a group, and they are no longer in any Groups, remove the configured role

This is the opposite of the available Group sync roles functionality, which will
take an outside role and confer ability into the Group.

It is possible achieve some things with Group roles, for example you can
control Views visibility with Group roles, however Group roles are not always
available.

## Update hook

This module won't bulk change user roles. You can use a install hook like this to align
all existing users.

```
use Drupal\user\Entity\User;

/**
 * Apply the automatic role if users have any group membership.
 */
function foo_update_10001() {
  $role_apply = \Drupal::service('group_member_role.automatic');
  $ids = \Drupal::entityQuery('user')->accessCheck(FALSE)->execute();
  $users = User::loadMultiple($ids);

  foreach ($users as $user) {
    // Safe to apply to any user.
    $role_apply->applyAutomaticRole($user);
  }
}
```
