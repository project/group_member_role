<?php

namespace Drupal\group_member_role\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group_member_role\GroupMemberRoleAutomatic;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements a GroupMemberRoleConfigForm form.
 */
class GroupMemberRoleConfigForm extends ConfigFormBase {

  /**
   * GroupMemberRoleAutomatic.
   *
   * @var \Drupal\group_member_role\GroupMemberRoleAutomatic
   */
  protected $groupMemberRoleAutomatic;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, GroupMemberRoleAutomatic $group_member_role_automatic) {
    $this->groupMemberRoleAutomatic = $group_member_role_automatic;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('group_member_role.automatic'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_member_role_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['group_member_role.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $current_conf = $this->configFactory->get('group_member_role.settings');

    $form['warning'] = [
      '#markup' => $this->t("Roles can be still be manually added or removed, however such manual changes will be overridden whenever the conditions below are triggered. <br />This module doesn't alter the user's status and does not currently support applying bulk changes. See README for tips."),
    ];

    $form['automatic_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Automatic role'),
      '#description' => $this->t('This role will be applied if the user has any memberships, and removed when the user has no more memberships.'),
      '#options' => $this->groupMemberRoleAutomatic->allowableRoles(),
      '#default_value' => $current_conf->get('automatic_role'),
    ];
    $form['skip_on_existing'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Skip on roles'),
      '#description' => $this->t('Do not give the user the automatic role if they already have any of these roles.'),
      '#options' => $this->groupMemberRoleAutomatic->assignableRoles(),
      '#default_value' => $current_conf->get('skip_on_existing'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory->getEditable('group_member_role.settings');
    $config->set('automatic_role', $form_state->getValue('automatic_role'));
    $skip_on_existing = array_filter($form_state->getValue('skip_on_existing'), function ($value) {
      // Ignore checkbox options which are not checked.
      return $value !== 0;
    });

    $config->set('skip_on_existing', $skip_on_existing);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
