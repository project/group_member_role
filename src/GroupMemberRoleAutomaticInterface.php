<?php

namespace Drupal\group_member_role;

use Drupal\user\UserInterface;

/**
 * GroupMemberRoleApply interface.
 */
interface GroupMemberRoleAutomaticInterface {

  /**
   * Determine if the passed in user is a member of any groups.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account to check.
   *
   * @return bool
   *   TRUE if the user account is a member of any groups.
   */
  public function hasAnyMembership(UserInterface $user): bool;

  /**
   * Push the configured roles onto the user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account to check.
   */
  public function applyAutomaticRole(UserInterface $user): void;

  /**
   * Remove the configured roles from the user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account to check.
   */
  public function removeAutomaticRole(UserInterface $user): void;

  /**
   * Returns the roles can be assigned, minus unsafe ones.
   *
   * @return array
   *   Array of role machine names.
   */
  public function allowableRoles(): array;

  /**
   * All roles that can be assigned.
   *
   * @return array
   *   Array of role machine names.
   */
  public function assignableRoles(): array;

  /**
   * Returns the currently configured role.
   *
   * @return string
   *   A configured role machine name.
   */
  public function getConfiguredRole(): string|FALSE;

}
