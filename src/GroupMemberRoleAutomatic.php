<?php

namespace Drupal\group_member_role;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\group\Entity\GroupMembership;
use Drupal\user\Entity\Role;
use Drupal\user\UserInterface;

/**
 * Apply and remove roles in reaction to users added to and removed from groups.
 */
class GroupMemberRoleAutomatic implements GroupMemberRoleAutomaticInterface {

  /**
   * Constructs a GroupMemberRoleApply object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   The logger channel service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LoggerChannelInterface $loggerChannel,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function hasAnyMembership(UserInterface $user): bool {
    return (bool) count(GroupMembership::loadByUser($user));
  }

  /**
   * {@inheritdoc}
   */
  public function applyAutomaticRole(UserInterface $user): void {
    $configured_role = $this->getConfiguredRole();

    if ($configured_role && !$this->userHasSkippedRole($user)) {
      if ($this->hasAnyMembership($user)) {
        $user->addRole($configured_role);
        $user->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeAutomaticRole(UserInterface $user): void {
    if (!$this->hasAnyMembership($user)) {
      $configured_role = $this->getConfiguredRole();
      if ($configured_role) {
        $user->removeRole($configured_role);
        $user->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function allowableRoles(): array {
    $roles = [];
    foreach ($this->entityTypeManager->getStorage('user_role')->loadMultiple() as $role) {
      if ($role->id() !== Role::ANONYMOUS_ID && $role->id() !== Role::AUTHENTICATED_ID && !$role->isAdmin()) {
        $roles[$role->id()] = $role->label();
      }
    }
    return $roles;
  }

  /**
   * {@inheritdoc}
   */
  public function assignableRoles(): array {
    $roles = [];
    foreach ($this->entityTypeManager->getStorage('user_role')->loadMultiple() as $role) {
      if ($role->id() !== Role::ANONYMOUS_ID && $role->id() !== Role::AUTHENTICATED_ID) {
        $roles[$role->id()] = $role->label();
      }
    }
    return $roles;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredRole(): string|FALSE {
    $role = $this->configFactory->get('group_member_role.settings')->get('automatic_role');
    if (isset($this->allowableRoles()[$role])) {
      return $role;
    }
    elseif (!empty($role)) {
      $this->loggerChannel->error('Configuration of group_member_role is trying to assign an automatic role that is invalid: @role.', ['@role' => (string) $role]);
    }
    return FALSE;
  }

  /**
   * Check if the current user has any of the roles configured as skipped roles.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account to check.
   *
   * @return bool
   *   TRUE if the passed in user any skipped role.
   */
  private function userHasSkippedRole($user): bool {
    foreach ($this->configFactory->get('group_member_role.settings')->get('skip_on_existing') as $rid) {
      if ($user->hasRole($rid)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
