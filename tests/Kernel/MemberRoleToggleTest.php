<?php

namespace Drupal\Tests\group_member_role\Kernel;

use Drupal\Tests\group\Kernel\GroupKernelTestBase;

/**
 * Tests module configuration.
 *
 * @group group
 */
class MemberRoleToggleTest extends GroupKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'group',
    'group_member_role',
  ];

  /**
   * The worker class.
   *
   * @var \Drupal\group_member_role\GroupMemberRoleAutomatic
   */
  protected $groupMemberRoleAutomatic;

  /**
   * The role automatically assigned and removed.
   *
   * @var \Drupal\user\Entity\Role
   */
  protected $memberRole;

  /**
   * The role that will cause the user to not be given the automatic role.
   *
   * @var \Drupal\user\Entity\Role
   */
  protected $otherRole;

  /**
   * A miscellaneous group.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected $group;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createGroupType(['id' => 'foo']);
    $this->installConfig('system');
    $this->installEntitySchema('user');
    $this->installConfig('group_member_role');
    $this->groupMemberRoleAutomatic = \Drupal::service('group_member_role.automatic');

    $role_storage = $this->entityTypeManager->getStorage('user_role');
    $this->memberRole = $role_storage->create(['id' => 'member', 'label' => 'Member']);
    $this->memberRole->save();
    $this->otherRole = $role_storage->create(['id' => 'other', 'label' => 'Other']);
    $this->otherRole->save();

    $this->config('group_member_role.settings')
      ->set('automatic_role', $this->memberRole->id())
      ->set('skip_on_existing', [$this->otherRole->id()])
      ->save();
  }

  /**
   * Test basic adding and removing user from group.
   */
  public function testMemberRoleToggle(): void {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $group_type = $this->createGroupType(['creator_membership' => FALSE]);
    $group = $this->createGroup([
      'type' => $group_type->id(),
    ]);

    $user = $this->createUser();
    $group->addMember($user);
    // The user we pass will not reflect changes, so we need to reload it.
    $user = $user_storage->load($user->id());
    $this->assertTrue($user->hasRole($this->memberRole->id()));
    $group->removeMember($user);
    $user = $user_storage->load($user->id());
    $this->assertFalse($user->hasRole($this->memberRole->id()));
  }

  /**
   * Test basic adding user when they have skipped role.
   */
  public function testSkippedRoleSkips(): void {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $group_type = $this->createGroupType(['creator_membership' => FALSE]);
    $group = $this->createGroup([
      'type' => $group_type->id(),
    ]);

    $user = $this->createUser();
    $user = $user->addRole($this->otherRole->id());
    $user->save();
    $group->addMember($user);
    $user = $user_storage->load($user->id());
    $this->assertFalse($user->hasRole($this->memberRole->id()));
  }

}
